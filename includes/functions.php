<?php 
defined( 'ABSPATH' ) || exit;

// register shortcode
add_shortcode('saleProducts', 'carousel'); 
function carousel() {
	if ( ! is_admin() ):
		//wp_enqueue_style('style_slider');
		$products = getProducts();
		$length_products = count($products);
		$content = '<!-- Item slider-->
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="carousel carousel-showmanymoveone slide" id="itemslider">
									<div class="carousel-inner">';
										foreach ($products as $product) {
											$content .= $product;
										}
				
						$content .= '</div>
									<div id="slider-control">
										<a class="left carousel-control" href="#itemslider" data-slide="prev"><img src="https://cdn0.iconfinder.com/data/icons/website-kit-2/512/icon_402-512.png" alt="Left" class="img-responsive"></a>
										<a class="right carousel-control" href="#itemslider" data-slide="next"><img src="http://pixsector.com/cache/81183b13/avcc910c4ee5888b858fe.png" alt="Right" class="img-responsive"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Item slider end-->';
	else: $content = null;
	endif;
	return $content;
}

function getProducts() {
	$ids = get_posts(array(
	'post_type'      => 'product',
		'numberposts' => -1,
		'post_status' => 'publish',
        'fields' => 'ids',
		'post__in' => wc_get_product_ids_on_sale()
	));
	$products = [];
	if (! empty($ids)) {
		foreach ( $ids as $id ) {
			$product = wc_get_product( $id );
			if ($id == $ids[0]){$content = '<div class="item active">';}
			else {$content = '<div class="item">';}
					$content .= '<div class="col-xs-12 col-sm-6 col-md-2">
									<a href="'.$product->get_permalink().'">'.$product->get_image().'
									<h4 class="text-center">'.$product->get_name().'</h4>';
									if ($product->get_type()=='variable') {
										$available_variations = $product->get_available_variations();
										$variation_id=$available_variations[0]['variation_id']; // Getting the variable id of just the 1st product. You can loop $available_variations to get info about each variation.
										$variable_product1= new WC_Product_Variation( $variation_id );
										$content .= '<del aria-hidden="true">'.wc_price($variable_product1->get_regular_price()).'</del> '.wc_price($variable_product1->get_sale_price()).'<small>'.$variable_product1->get_price_suffix().'</small>';
										$content .= '</a><a href="'.$variable_product1->add_to_cart_url().'" rel="nofollow" data-product_id="'.$variable_product1->get_id().'" data-product_sku="'.$variable_product1->get_sku().'" class="button add_to_cart_button product_type_'.$variable_product1->get_type().'">'.$variable_product1->add_to_cart_text().'</a>';
									}
									else {
										$content .= '<del aria-hidden="true">'.wc_price($product->get_regular_price()).'</del> '.wc_price($product->get_sale_price()).'<small>'.$product->get_price_suffix().'</small>';
										$content .= '</a><a href="'.$product->add_to_cart_url().'" rel="nofollow" data-product_id="'.$product->get_id().'" data-product_sku="'.$product->get_sku().'" class="button add_to_cart_button product_type_'.$product->get_type().'">'.$product->add_to_cart_text().'</a>';
									}
					$content .= '</div>
							</div>';
		$products[] = $content;
	   }
	}
	return $products;
}
add_action('wp_enqueue_scripts', 'callback_for_setting_up_scripts');
function callback_for_setting_up_scripts() {
    wp_register_style( 'style_slider', plugins_url( 'css/public.css', __FILE__ ) );
    wp_enqueue_style( 'style_slider' );
    wp_enqueue_script( 'slider-controls', plugins_url( 'js/slidershow-controls.js', __FILE__ ), array( 'jquery' ) );
	wp_register_style( 'Bootstrap.css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', __FILE__  );
	wp_register_style( 'BootstrapTheme.css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css', __FILE__  );
    wp_enqueue_style( 'Bootstrap.css' );
    wp_enqueue_style( 'BootstrapTheme.css' );
    wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', __FILE__ , array( 'jquery' ) );
    wp_enqueue_script( 'Bootstrap.js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', __FILE__ , array( 'jquery' ) );
}
?>
