<?php
/**
 * Plugin Name: Product slider
 * Plugin URI: https://kiwi-computing.dk/plugins/product-slider
 * Description: A WooCommerce Product Slider Carousel
 * Version: 1.0
 * Author: Kiwi Computing
 * Author URI: https://kiwi-computing.dk/
 * License: GPLv3
 * License URI: https://opensource.org/licenses/GPL-3.0
 * Text Domain: product-slider
 */
defined( 'ABSPATH' ) || exit;

require (__DIR__ .'/includes/loader.php');
require (__DIR__ .'/uninstall.php');
